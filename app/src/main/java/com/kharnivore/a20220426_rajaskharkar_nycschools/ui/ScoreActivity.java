package com.kharnivore.a20220426_rajaskharkar_nycschools.ui;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kharnivore.a20220426_rajaskharkar_nycschools.R;

import static com.kharnivore.a20220426_rajaskharkar_nycschools.ui.MainActivity.MATH_SCORE;
import static com.kharnivore.a20220426_rajaskharkar_nycschools.ui.MainActivity.READING_SCORE;
import static com.kharnivore.a20220426_rajaskharkar_nycschools.ui.MainActivity.SCHOOL_NAME;
import static com.kharnivore.a20220426_rajaskharkar_nycschools.ui.MainActivity.STUDENT_COUNT;
import static com.kharnivore.a20220426_rajaskharkar_nycschools.ui.MainActivity.WRITING_SCORE;

public class ScoreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);
        //Grabbing intent data and displaying in textViews
        TextView schoolNameTextView = findViewById(R.id.schoolNameScoreTV);
        TextView mathScoreTextView = findViewById(R.id.mathScoreTV);
        TextView readingScoreTextView = findViewById(R.id.readingScoreTV);
        TextView writingScoreTextView = findViewById(R.id.writingScoreTV);
        TextView studentCountTextView = findViewById(R.id.studentCountTV);
        Intent intent = getIntent();
        String schoolName = intent.getStringExtra(SCHOOL_NAME);
        String mathScore = intent.getStringExtra(MATH_SCORE);
        String readingScore = intent.getStringExtra(READING_SCORE);
        String writingScore = intent.getStringExtra(WRITING_SCORE);
        String studentCount = intent.getStringExtra(STUDENT_COUNT);
        schoolNameTextView.setText(schoolName);
        //Would call the strings below from strings.xml but getting an odd error with garbage values
        mathScoreTextView.setText("Math average: " + mathScore);
        readingScoreTextView.setText("Reading Average: " + readingScore);
        writingScoreTextView.setText("Writing Average: " + writingScore);
        studentCountTextView.setText("Student Count: "+ studentCount);
    }
}
