package com.kharnivore.a20220426_rajaskharkar_nycschools.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import com.kharnivore.a20220426_rajaskharkar_nycschools.R;
import com.kharnivore.a20220426_rajaskharkar_nycschools.adapter.SchoolsRecyclerViewAdapter;
import com.kharnivore.a20220426_rajaskharkar_nycschools.calls.GetJsonData;
import com.kharnivore.a20220426_rajaskharkar_nycschools.models.School;
import com.kharnivore.a20220426_rajaskharkar_nycschools.models.ScoreData;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity
        extends AppCompatActivity
        implements SchoolsRecyclerViewAdapter.ListItemClickListener {

    private static final String BASE_URL = "https://data.cityofnewyork.us/resource/";
    public static final String SCHOOL_NAME = "name";
    public static final String STUDENT_COUNT = "count";
    public static final String MATH_SCORE = "math_score";
    public static final String READING_SCORE = "reading_score";
    public static final String WRITING_SCORE = "writing_score";

    private static RecyclerView recyclerView;
    GetJsonData getJsonData;
    List<School> schoolList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Using RecyclerView to display school names
        recyclerView = findViewById(R.id.schoolsRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        makeCallForSchoolNames();
    }

    private void makeCallForSchoolNames() {
        //Using Retrofit to get school data
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        getJsonData = retrofit.create(GetJsonData.class);
        Call<List<School>> call = getJsonData.getSchools();
        call.enqueue(new Callback<List<School>>() {

            @Override
            public void onResponse(Call<List<School>> call, Response<List<School>> response) {
                if(!response.isSuccessful()) {
                    Toast.makeText(
                            MainActivity.this,
                            response.body().toString(),
                            Toast.LENGTH_SHORT
                    ).show();
                    return;
                }
                schoolList = response.body();
                SchoolsRecyclerViewAdapter schoolsRecyclerViewAdapter
                        = new SchoolsRecyclerViewAdapter(
                                schoolList,
                        MainActivity.this::onListItemClick
                );
                recyclerView.setAdapter(schoolsRecyclerViewAdapter);
            }

            @Override
            public void onFailure(Call<List<School>> call, Throwable t) {
                Toast.makeText(
                        MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT
                ).show();
            }
        });
    }

    @Override
    public void onListItemClick(int position) {
        School school = schoolList.get(position);
        //Using dbn to get corresponding score data
        String dbn = school.dbn;
        if(dbn != null && !dbn.equals("")) {
            makeCallForScoreData(dbn);
        }
    }

    private void makeCallForScoreData(String dbn) {
        Call<List<ScoreData>> call = getJsonData.getScore(dbn);
        call.enqueue(new Callback<List<ScoreData>>() {
            @Override
            public void onResponse(Call<List<ScoreData>> call, Response<List<ScoreData>> response) {
                if(response.body().size() == 0) {
                    Toast.makeText(
                            MainActivity.this,
                            "Data unavailable.",
                            Toast.LENGTH_SHORT
                    ).show();
                    return;
                }
                ScoreData scoreData= response.body().get(0);
                Intent intent = new Intent(MainActivity.this, ScoreActivity.class);
                intent.putExtra(SCHOOL_NAME, scoreData.schoolName);
                intent.putExtra(STUDENT_COUNT, scoreData.studentCount);
                intent.putExtra(MATH_SCORE, scoreData.mathScore);
                intent.putExtra(READING_SCORE, scoreData.readingScore);
                intent.putExtra(WRITING_SCORE, scoreData.writingScore);
                startActivity(intent);
            }

            @Override
            public void onFailure(Call<List<ScoreData>> call, Throwable t) {
                Toast.makeText(
                        MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT
                ).show();
            }
        });
    }
}