package com.kharnivore.a20220426_rajaskharkar_nycschools.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.kharnivore.a20220426_rajaskharkar_nycschools.R;
import com.kharnivore.a20220426_rajaskharkar_nycschools.models.School;

import java.util.List;

public class SchoolsRecyclerViewAdapter
        extends RecyclerView.Adapter<SchoolsRecyclerViewAdapter.ViewHolder> {

    private List<School> schoolObjects;
    final private ListItemClickListener mOnClickListener;

    public SchoolsRecyclerViewAdapter(
            List<School> schoolObjects,
            ListItemClickListener mOnClickListener
    ) {
        this.schoolObjects = schoolObjects;
        this.mOnClickListener = mOnClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.school_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        School school = schoolObjects.get(position);
        String name = school.schoolName;
        holder.schoolNameTextView.setText(name);
    }

    @Override
    public int getItemCount() {
        return schoolObjects.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView schoolNameTextView;

        public ViewHolder(View view) {
            super(view);
            itemView.setOnClickListener(this);
            schoolNameTextView = view.findViewById(R.id.schoolNameTV);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            mOnClickListener.onListItemClick(position);
        }
    }

    public interface ListItemClickListener {
        void onListItemClick (int position);
    }
}
