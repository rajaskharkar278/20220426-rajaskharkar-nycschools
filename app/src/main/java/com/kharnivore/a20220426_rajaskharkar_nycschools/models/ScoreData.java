package com.kharnivore.a20220426_rajaskharkar_nycschools.models;

import com.google.gson.annotations.SerializedName;

public class ScoreData {

    @SerializedName("dbn")
    public String dbn;

    @SerializedName("school_name")
    public String schoolName;

    @SerializedName("num_of_sat_test_takers")
    public String studentCount;

    @SerializedName("sat_critical_reading_avg_score")
    public String readingScore;

    @SerializedName("sat_math_avg_score")
    public String mathScore;

    @SerializedName("sat_writing_avg_score")
    public String writingScore;
}
