package com.kharnivore.a20220426_rajaskharkar_nycschools.models;

import com.google.gson.annotations.SerializedName;

public class School {

    @SerializedName("school_name")
    public String schoolName;

    @SerializedName("dbn")
    public String dbn;
}
