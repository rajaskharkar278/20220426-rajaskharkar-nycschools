package com.kharnivore.a20220426_rajaskharkar_nycschools.calls;

import com.kharnivore.a20220426_rajaskharkar_nycschools.models.School;
import com.kharnivore.a20220426_rajaskharkar_nycschools.models.ScoreData;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GetJsonData {

    @GET("s3k6-pzi2.json")
    Call<List<School>> getSchools();

    @GET("f9bf-2cp4.json")
    Call<List<ScoreData>> getScore(@Query("dbn") String dbn);
}
